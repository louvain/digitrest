package org.proto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitProxyApplication.class, args);
	}

}
