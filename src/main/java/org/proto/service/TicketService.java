package org.proto.service;

import java.util.logging.Logger;

import org.proto.TicketStream;
import org.proto.dto.TicketRequest;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

@Service
public class TicketService {

	private final TicketStream ticketStream;

    Logger log = Logger.getLogger(TicketService.class.getName());
    
    public TicketService(TicketStream ticketStream) {
        this.ticketStream = ticketStream;
    }

    public void sendTicket(final TicketRequest ticketRequest) {
        log.info("Sending Ticket " + ticketRequest);

        MessageChannel messageChannel = ticketStream.outboundTicket();
        messageChannel.send(MessageBuilder
                .withPayload(ticketRequest)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }
	
}
