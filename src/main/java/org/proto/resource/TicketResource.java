package org.proto.resource;

import javax.validation.Valid;

import org.proto.dto.TicketRequest;
import org.proto.dto.TicketResponse;
import org.proto.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/tickets")
public class TicketResource {
	
	@Autowired
	TicketService ticketService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Mono<TicketResponse> createTicket(@Valid @RequestBody
			TicketRequest ticketRequest) {
		
		try {
			long id = _generateId();
			ticketRequest.setId(id);
			
			ticketService.sendTicket(ticketRequest);
			
			TicketResponse response = new TicketResponse(id,"created");
			
			return Mono.just(response);
			
		} catch (Exception e) {
			// Sould return 500
		}
		return Mono.empty();
		
	}
	
	private long _generateId() {
		return System.currentTimeMillis();
	}
}
