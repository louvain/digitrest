package org.proto;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(TicketStream.class)
public class StreamsConfig {

}
