package org.proto.dto;

import java.util.List;

import javax.validation.Valid;

public class TicketRequest {

	private long id;
	
	private Provider provider;
	@Valid
	private Person person;
	private List<Adress> adresses;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public Provider getProvier() {
		return provider;
	}
	public void setProvier(Provider provier) {
		this.provider = provier;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public List<Adress> getAdresses() {
		return adresses;
	}
	public void setAdresses(List<Adress> adresses) {
		this.adresses = adresses;
	}

	
	
}
