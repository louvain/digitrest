package org.proto.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class Person {

	@NotEmpty
	private String firstName;
	private String lastName;
	private LocalDate birthDate;
	private String nationality;
	private String gender;
	
	// @Pattern(regexp =  "^(\\d{9})(\\d(2))$")
	private String nationalRegister;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNationalRegister() {
		return nationalRegister;
	}
	public void setNationalRegister(String nationalRegister) {
		this.nationalRegister = nationalRegister;
	}
		
	
}
