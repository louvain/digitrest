package org.proto;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface TicketStream {

	String OUTPUT = "ticket-out";
	
	@Output(OUTPUT)
	MessageChannel outboundTicket();
}
